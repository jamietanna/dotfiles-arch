-- vim: ts=2 sts=2 sw=2 et
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable',
    lazypath,
  }
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup({
  {
    'neovim/nvim-lspconfig',
    dependencies = {
      -- Automatically install LSPs to stdpath for neovim
      { 'williamboman/mason.nvim', config = true },
      'williamboman/mason-lspconfig.nvim',

      -- Useful status updates for LSP
      {
        "j-hui/fidget.nvim",
        tag = "legacy",
        -- opts = {},
      },

      -- Additional lua configuration, makes nvim stuff amazing!
      'folke/neodev.nvim',
    },
    config = function()
      vim.api.nvim_create_autocmd("BufWritePre", {
        callback = function()
          if not vim.b.disable_autoformat == true then
            vim.lsp.buf.format()
          end
        end
      })

      vim.api.nvim_create_user_command('DisableAutoformat', function()
        if vim.b.disable_autoformat == nil then
          vim.b.disable_autoformat = true
        else
          vim.b.disable_autoformat = nil
        end
      end, { desc = 'Toggle whether the LSP autoformatter should be used' })
    end,
  },

  {
    "ray-x/lsp_signature.nvim",
    event = "VeryLazy",
    opts = {},
    config = function(_, opts) require 'lsp_signature'.setup(opts) end
  },

  {
    'lewis6991/gitsigns.nvim',
    opts = {
      signs = {
        add = { text = '+' },
        change = { text = '~' },
        delete = { text = '_' },
        topdelete = { text = '‾' },
        changedelete = { text = '~' },
      },
    },
  },

  {
    'srcery-colors/srcery-vim',
    priority = 1000,
    config = function()
      vim.g.srcery_italic = 1
      vim.cmd.colorscheme 'srcery'
      vim.o.background = 'dark'
      vim.o.termguicolors = true
    end,
  },

  {
    -- Add indentation guides even on blank lines
    'lukas-reineke/indent-blankline.nvim',
    main = "ibl",
    opts = {
      -- char = '┊',
      -- show_trailing_blankline_indent = false,
    },
  },

  -- "gc" to comment visual regions/lines
  { 'numToStr/Comment.nvim', opts = {} },

  -- Fuzzy Finder (files, lsp, etc)
  {
    'nvim-telescope/telescope.nvim',
    version = '*',
    dependencies = {
      'nvim-lua/plenary.nvim'
    }
  },

  {
    'nvim-telescope/telescope-fzf-native.nvim',
    -- NOTE: If you are having trouble with this installation,
    --       refer to the README for telescope-fzf-native for more instructions.
    build = 'make',
    cond = function()
      return vim.fn.executable 'make' == 1
    end,
  },

  {
    'nvim-treesitter/nvim-treesitter',
    dependencies = {
      'nvim-treesitter/nvim-treesitter-textobjects',
    },
    build = ":TSUpdate",
  },

  {
    'nanotee/sqls.nvim',
  },

  {
    'https://gitlab.com/deps.fyi/dmd.nvim',
    config = function()
      require('dmd')
    end
  },

  {
    import = 'custom.plugins'
  },
}, {})

vim.o.hlsearch = true
vim.keymap.set('n', '<leader><CR>', '<cmd>:nohlsearch<cr>')

vim.wo.relativenumber = true

-- Enable mouse mode
vim.o.mouse = ''

-- Save undo history
vim.o.undofile = true

vim.o.ignorecase = true
vim.o.smartcase = true

vim.wo.signcolumn = 'yes'

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeout = true
vim.o.timeoutlen = 500

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

-- TODO
-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- Remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})

require('telescope').setup {
  defaults = {
    mappings = {
      i = {
        ['<C-u>'] = false,
        ['<C-d>'] = false,
      },
    },
  },
}

-- Enable telescope fzf native, if installed
pcall(require('telescope').load_extension, 'fzf')

-- See `:help telescope.builtin`
vim.keymap.set('n', '<leader>?', require('telescope.builtin').oldfiles, { desc = '[?] Find recently opened files' })
vim.keymap.set('n', '<leader><space>', require('telescope.builtin').buffers, { desc = '[ ] Find existing buffers' })
vim.keymap.set('n', '<leader>/', function()
  -- You can pass additional configuration to telescope to change theme, layout, etc.
  require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
    winblend = 10,
    previewer = false,
  })
end, { desc = '[/] Fuzzily search in current buffer' })

vim.keymap.set('n', '<leader>gf', require('telescope.builtin').git_files, { desc = 'Search [G]it [F]iles' })
vim.keymap.set('n', '<leader>gF', require('telescope.builtin').git_status, { desc = 'Search [G]it [F]iles' })
vim.keymap.set('n', '<leader>sf', require('telescope.builtin').find_files, { desc = '[S]earch [F]iles' })
vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags, { desc = '[S]earch [H]elp' })
vim.keymap.set('n', '<leader>sw', require('telescope.builtin').grep_string, { desc = '[S]earch current [W]ord' })
vim.keymap.set('n', '<leader>sg', require('telescope.builtin').live_grep, { desc = '[S]earch by [G]rep' })
vim.keymap.set('n', '<leader>sd', require('telescope.builtin').diagnostics, { desc = '[S]earch [D]iagnostics' })

-- [[ Configure Treesitter ]]
-- See `:help nvim-treesitter`
require('nvim-treesitter.configs').setup {
  -- Add languages to be installed here that you want installed for treesitter
  ensure_installed = {
    'comment',
    'go',
    'lua',
    'python',
    'rust',
    'tsx',
    'typescript',
    'vimdoc',
    'vim'
  },

  auto_install = false,

  highlight = { enable = true },
  indent = { enable = true, disable = { 'python' } },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = '<c-space>',
      node_incremental = '<c-space>',
      scope_incremental = '<c-s>',
      node_decremental = '<M-space>',
    },
  },
  textobjects = {
    select = {
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        -- You can use the capture groups defined in textobjects.scm
        ['aa'] = '@parameter.outer',
        ['ia'] = '@parameter.inner',
        ['af'] = '@function.outer',
        ['if'] = '@function.inner',
        ['ac'] = '@class.outer',
        ['ic'] = '@class.inner',
      },
    },
    move = {
      enable = true,
      set_jumps = true, -- whether to set jumps in the jumplist
      goto_next_start = {
        [']m'] = '@function.outer',
        [']]'] = '@class.outer',
      },
      goto_next_end = {
        [']M'] = '@function.outer',
        [']['] = '@class.outer',
      },
      goto_previous_start = {
        ['[m'] = '@function.outer',
        ['[['] = '@class.outer',
      },
      goto_previous_end = {
        ['[M'] = '@function.outer',
        ['[]'] = '@class.outer',
      },
    },
    swap = {
      enable = true,
      swap_next = {
        ['<leader>a'] = '@parameter.inner',
      },
      swap_previous = {
        ['<leader>A'] = '@parameter.inner',
      },
    },
  },
}

-- Diagnostic keymaps
vim.keymap.set('n', '<leader>,', vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic message" })
vim.keymap.set('n', '<leader>.', vim.diagnostic.goto_next, { desc = "Go to next diagnostic message" })
vim.keymap.set('n', '<leader>l', vim.diagnostic.open_float, { desc = "Open floating diagnostic message" })
vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = "Open diagnostics list" })

-- LSP settings.
--  This function gets run when an LSP connects to a particular buffer.
local on_attach = function(client, bufnr)
  require('sqls').on_attach(client, bufnr)

  local nmap = function(keys, func, desc)
    if desc then
      desc = 'LSP: ' .. desc
    end

    vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
  end

  nmap('<leader>r', vim.lsp.buf.rename, '[R]ename')

  nmap('K', vim.lsp.buf.hover, '')

  nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')
  nmap('<leader>d', vim.lsp.buf.code_action, '')
  -- TODO
  -- nmap('<C-]>', vim.lsp.buf.definition, '[G]oto [D]efinition')
  nmap('gd', vim.lsp.buf.definition, '[G]oto [D]efinition')
  -- TODO

  -- nmap('gr', require('telescope.builtin').lsp_references, '[G]oto [R]eferences')
  -- nmap('gI', vim.lsp.buf.implementation, '[G]oto [I]mplementation')
  -- nmap('<leader>D', vim.lsp.buf.type_definition, 'Type [D]efinition')
  -- nmap('<leader>ds', require('telescope.builtin').lsp_document_symbols, '[D]ocument [S]ymbols')
  -- nmap('<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols, '[W]orkspace [S]ymbols')
  --
  -- -- See `:help K` for why this keymap
  --
  -- -- Lesser used LSP functionality
  -- nmap('gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')
  -- nmap('<leader>wa', vim.lsp.buf.add_workspace_folder, '[W]orkspace [A]dd Folder')
  -- nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
  -- nmap('<leader>wl', function()
  --   print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  -- end, '[W]orkspace [L]ist Folders')
  --
  -- Create a command `:Format` local to the LSP buffer
  vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
    vim.lsp.buf.format()
  end, { desc = 'Format current buffer with LSP' })
end

local servers = {
  gopls = {},
  golangci_lint_ls = {},
  tsserver = {},
  solargraph = {},
  jsonls = {},
  html = {
    html = {
      format = {
        -- to effectively disable it
        wrapLineLength = 10000,
      }
    }
  },
  vimls = {},
  terraformls = {},
  yamlls = {
    yaml = {
      keyOrdering = false,
      validate = true,
      completion = true,
    },
  },

  -- TODO add EFM for ShellCheck on scripts

  lua_ls = {
    Lua = {
      workspace = { checkThirdParty = false },
      telemetry = { enable = false },
    },
  },

  sqls = {},
}

require('neodev').setup()

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Ensure the servers above are installed
local mason_lspconfig = require 'mason-lspconfig'

mason_lspconfig.setup {
  ensure_installed = vim.tbl_keys(servers),
}

mason_lspconfig.setup_handlers {
  function(server_name)
    require('lspconfig')[server_name].setup {
      capabilities = capabilities,
      on_attach = on_attach,
      settings = servers[server_name],
    }
  end,
}

-- when pasting from external sources, it's great to be able to (un)set `paste`
-- to not mess around with line endings and wrapping
vim.keymap.set('n', '<leader>p', '<cmd>:set paste!<cr>')

-- Paste from the Clipboard, converting HTML to Markdown
-- Via https://www.jvt.me/posts/2025/01/26/linux-html-clipboard/ and https://stackoverflow.com/a/24495620
-- Using the standard, consistent CommonMark, instead of Pandoc's flavour
vim.keymap.set('n', '<leader>P', '<cmd>r !xclip  -o -t text/html | pandoc -f html -t commonmark+hard_line_breaks<cr>')

vim.keymap.set('n', '<leader>y', '"+y')

vim.keymap.set('n', '<leader><', '<cmd>:cprev<cr>')
vim.keymap.set('n', '<leader>>', '<cmd>:cnext<cr>')

-- remove trailing whitespace on write
vim.cmd [[autocmd BufWritePre * :%s/\s\+$//e]]

vim.keymap.set('v', '<leader>u', "y0\"_DpV:'<,'>!unpack<CR>")

-- TODO
-- TODO
-- TODO
vim.cmd [[
" https://certitude.consulting/blog/en/invisible-backdoor/
" https://stackoverflow.com/a/16988346/2257038
" syntax match nonascii "[^\x00-\x7F]"
"
" syntax match smartquotes "[‘’“”]"
" syntax match nonascii /\<\l\+\>/
syntax match nonascii "[^\x00-\x7F]"
highlight nonascii guibg=Red ctermbg=1
" %s/[‘’]/'/g
" %s/[“”]/"/g
syntax match smartquotes "[‘’“”]"
highlight! smartquotes guibg=Red ctermbg=1
]]
-- -- vim.cmd.syntax({ "match", "smartquotes", '"[‘’“”]"' })
-- vim.cmd.syntax [[ match smartquotes "[‘’“”]" ]]
-- -- vim.cmd.highlight({ "smartquotes", "bg=Red", "ctermbg=1" })
-- vim.cmd.highlight({ "smartquotes", "guibg=red", "ctermbg=1" })

-- https://www.reddit.com/r/neovim/comments/1172kfi/migrate_vimscriptbased_custom_syntax_highlighting/

-- https://neovim.io/doc/user/api.html#nvim_set_hl()
-- vim.api.nvim_set_hl(0, 'nonascii', { bg = 'Red', ctermbg = 1 })
-- vim.api.nvim_set_hl(0, 'smartquotes', { bg = 'Red', ctermbg = 1 })
-- TODO
-- TODO
-- TODO


vim.o.tabstop = 2
vim.o.shiftwidth = 2
