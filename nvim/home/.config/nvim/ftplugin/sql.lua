-- Via https://github.com/sqls-server/sqls/issues/149 we can't use autoformat as it'll remove all whitespace
vim.b.disable_autoformat = true

vim.keymap.set('n', '<leader>D', ':DMDDatasette<CR>')
vim.keymap.set('n', '<leader>x', ':SqlsExecuteQuery <CR>')

-- Due to https://github.com/sqls-server/sqls/issues/149
vim.g.formatexpr = ''
