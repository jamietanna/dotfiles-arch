local function config()
  require("dmd.cmp")
  require("jvt_me.cmp")

  local cmp = require 'cmp'
  local luasnip = require 'luasnip'

  luasnip.config.setup {}
  require("luasnip.loaders.from_snipmate").lazy_load()

  cmp.setup {
    snippet = {
      expand = function(args)
        luasnip.lsp_expand(args.body)
      end,
    },
    mapping = cmp.mapping.preset.insert {
      ['<C-n>'] = cmp.mapping.select_next_item(),
      ['<C-p>'] = cmp.mapping.select_prev_item(),
      ['<C-Space>'] = cmp.mapping.complete {},
      ['<Tab>'] = cmp.mapping.confirm {
        behavior = cmp.ConfirmBehavior.Replace,
        select = true,
      },
      ['<S-Tab>'] = cmp.mapping(function(fallback)
        if cmp.visible() then
          cmp.select_prev_item()
        elseif luasnip.jumpable(-1) then
          luasnip.jump(-1)
        else
          fallback()
        end
      end, { 'i', 's' }),
    },
    sources = {
      { name = 'nvim_lsp' },
      { name = 'luasnip' },

      { name = 'dmd' },
      { name = 'jvt.me' },
    },
    experimental = {
      ghost_text = true,
    },
  }
end

return {
  'hrsh7th/nvim-cmp',
  dependencies = {
    'hrsh7th/cmp-nvim-lsp',
    'L3MON4D3/LuaSnip',
    'saadparwaiz1/cmp_luasnip',

    'honza/vim-snippets',

    {
      'https://gitlab.com/jamietanna/jvt.me.nvim',
      config = function()
        require('jvt_me')
      end,
    },
  },
  config = config,
}
