-- allow us to save a file if we forgot to open with sudo
return {
  'lambdalisue/suda.vim',
  config = function ()
    vim.keymap.set('c', 'w!!', '<cmd>:SudaWrite<CR>')
  end,
}
