return {
  'glacambre/firenvim',
  config = function()
    vim.g.firenvim_config = {
      ['localSettings'] = {
        ['.*'] = {
          ['takeover'] = 'never'
        }
      },
      ['globalSettings'] = {
        ['<C-n>'] = 'default',
      }
    }
  end
}
