return {
  'tpope/vim-projectionist',
  config = function()
    vim.g.projectionist_heuristics = {
      ['*.go'] = {
        ['*.go'] = {
          alternate = '{}_test.go',
          type = 'source',
          template = {
            'package {basename|camelcase}',
          },
        },
        ['*_test.go'] = {
          alternate = '{}.go',
          type = 'test',
          template = {
            'package {basename|camelcase}',
          },
        },
      },
      ['*.sql'] = {
        ['schema.sql'] = {
          alternate = 'queries.sql',
        },
        ['queries.sql'] = {
          alternate = 'schema.sql',
        }
      }
    }
  end,
}
