package main

import (
	"errors"
	"log"
	"net/http"
	"os"

	"willnorris.com/go/webmention"
)

func main() {
	log.SetPrefix("")

	client := webmention.New(http.DefaultClient)

	if len(os.Args) != 3 {
		log.Fatal("Usage: wm <source> <target>")
	}

	source := os.Args[1]
	target := os.Args[2]

	endpoint, err := client.DiscoverEndpoint(target)
	if errors.Is(err, webmention.ErrNoEndpointFound) {
		log.Printf("The target %v has no Webmention support", target)
		os.Exit(0)
	} else if err != nil {
		log.Fatalf("Could not discover Webmention endpoint for %v: %v", target, err)
	}

	resp, err := client.SendWebmention(endpoint, source, target)
	if err != nil {
		log.Fatalf("Could not send Webmention to %v: %v", target, err)
	}

	if resp.StatusCode != http.StatusCreated && resp.StatusCode != http.StatusAccepted {
		log.Printf("Received an HTTP %d instead of the expected HTTP 201 Created / HTTP 202 Accepted", resp.StatusCode)
	} else {
		if loc := resp.Header.Get("location"); loc != "" {
			log.Printf("Location for processing returned: %v", loc)
		}
	}
}
