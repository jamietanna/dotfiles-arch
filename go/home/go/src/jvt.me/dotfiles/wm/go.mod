module gitlab.com/jamietanna/dotfiles-arch/go/home/go/src/jvt.me/dotfiles/wm

go 1.22.0

require willnorris.com/go/webmention v0.0.0-20220108183051-4a23794272f0

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	golang.org/x/net v0.0.0-20211020060615-d418f374d309 // indirect
)
