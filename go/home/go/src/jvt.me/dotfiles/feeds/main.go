package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/foolin/pagser"
)

// https://stackoverflow.com/a/30112550/2257038
type html struct {
	LinkAlternate []struct {
		Href string `pagser:"->attr(href)" json:"feedUrl"`
		Type string `pagser:"->attr(type)" json:"feedType"`
	} `pagser:"head link[rel=alternate]"`
}

type result struct {
	Href string `json:"feedUrl"`
	Type string `json:"feedType"`
}

func PrettyString(str []byte) (string, error) {
	var prettyJSON bytes.Buffer
	if err := json.Indent(&prettyJSON, str, "", "  "); err != nil {
		return "", err
	}
	return prettyJSON.String(), nil
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: feeds [url]")
		os.Exit(1)
	}

	rawUrl := os.Args[1]
	if !strings.HasPrefix(rawUrl, "http://") && !strings.HasPrefix(rawUrl, "https://") {
		rawUrl = "http://" + rawUrl
	}

	u, err := url.Parse(rawUrl)
	if err != nil {
		fmt.Println("Invalid URL provided: ", err)
		os.Exit(1)
	}

	resp, err := http.Get(u.String())
	if err != nil {
		fmt.Println("Failed to fetch URL ", u, ":", err)
		os.Exit(1)
	}
	defer resp.Body.Close()
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		fmt.Println("Received an HTTP ", resp.StatusCode, " status code")
		os.Exit(1)
	}

	rawPageHtml, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Failed to parse response body: %s", err)
		os.Exit(1)
	}

	h := html{}
	p := pagser.New()

	err = p.Parse(&h, string(rawPageHtml))
	if err != nil {
		fmt.Println("Failed to parse response as HTML: %s", err)
		os.Exit(1)
	}

	var results []result
	for _, link := range h.LinkAlternate {
		uHref, err := url.Parse(link.Href)
		if err != nil {
			continue
		}
		result := result{
			Href: resp.Request.URL.ResolveReference(uHref).String(),
			Type: link.Type,
		}
		results = append(results, result)
	}

	bytes, err := json.Marshal(results)
	if err != nil {
		fmt.Println("Failed to marshal JSON", err)
		os.Exit(1)
	}
	str, err := PrettyString(bytes)
	if err != nil {
		fmt.Println("Failed to marshal JSON", err)
		os.Exit(1)
	}

	fmt.Println(str)
}
