module jvt.me/dotfiles/feeds

go 1.18

require github.com/foolin/pagser v0.1.5

require (
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/andybalholm/cascadia v1.1.0 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2 // indirect
)
