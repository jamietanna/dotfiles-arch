package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/url"
	"os"
	"strings"
)

type UrlResponse struct {
	Base        string              `json:"base"`
	Uri         string              `json:"uri"`
	Path        string              `json:"path"`
	RawFragment string              `json:"rawFragment"`
	Query       map[string][]string `json:"query"`
	Fragment    map[string][]string `json:"fragment"`
}

func ArgfRead() (string, error) {
	var bytes []byte
	var err error

	if len(os.Args) >= 2 {
		bytes, err = os.ReadFile(os.Args[1])
	} else {
		bytes, err = io.ReadAll(os.Stdin)
	}

	if err != nil {
		return "", err
	}
	return strings.TrimSuffix(string(bytes), "\n"), nil
}

func parse(s string) map[string][]string {
	q, err := url.ParseQuery(s)
	if err != nil {
		return make(map[string][]string)
	}
	return q
}

type BufferContainer struct {
	Type string `json:"type"`
	Data []byte `json:"data"`
}

func main() {
	input_bytes, err := ArgfRead()
	if err != nil {
		os.Exit(1)
	}
	input := strings.TrimSuffix(string(input_bytes), "\n")
	if len(input) == 0 {
		os.Exit(1)
	}
	var container BufferContainer
	err = json.Unmarshal([]byte(input_bytes), &container)
	if err != nil {
		os.Exit(1)
	}
	fmt.Println(string(container.Data))
}
