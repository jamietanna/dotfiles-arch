package main

import (
	"bytes"
	"encoding/json"
	"log"
	"os"
	"path/filepath"

	"golang.org/x/sync/errgroup"
)

func prettyPrint(filename string) error {
	data, err := os.ReadFile(filename)
	if err != nil {
		return err
	}

	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, data, "", "  ")
	if err != nil {
		return err
	}

	out := prettyJSON.Bytes()
	out = append(out, '\n')

	return os.WriteFile(filename, out, os.ModePerm)
}

func main() {
	if len(os.Args) == 1 {
		log.Fatal("Provide path(s) to JSON files to inline pretty-print")
	}

	files := os.Args[1:]

	processed := false

	var eg errgroup.Group
	for _, f := range files {
		matches, err := filepath.Glob(f)
		must(err)

		for _, match := range matches {
			match := match

			processed = true

			eg.Go(func() error {
				return prettyPrint(match)
			})
		}
	}

	if !processed {
		log.Fatal("Provide path(s) to JSON files to inline pretty-print")
	}

	must(eg.Wait())
}

func must(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
