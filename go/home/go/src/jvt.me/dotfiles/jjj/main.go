package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"log"
	"os"
	"path/filepath"

	"golang.org/x/sync/errgroup"
)

func prettyPrint(filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Split(bufio.ScanLines)

	// via https://stackoverflow.com/a/37455465, make sure we can handle lines up to 10MB
	buf := make([]byte, 0, bufio.MaxScanTokenSize)
	scanner.Buffer(buf, 1024*1024*10)

	var out []byte
	for scanner.Scan() {
		if out == nil {
			out = append(out, '[')
			out = append(out, '\n')
		} else {
			out = append(out, ',')
			out = append(out, '\n')
		}

		var prettyJSON bytes.Buffer
		err = json.Indent(&prettyJSON, scanner.Bytes(), "", "  ")
		if err != nil {
			return err
		}

		out = append(out, prettyJSON.Bytes()...)
	}

	if scanner.Err() != nil {
		return scanner.Err()
	}

	out = append(out, '\n')
	out = append(out, ']')

	return os.WriteFile(filename, out, os.ModePerm)
}

func main() {
	if len(os.Args) == 1 {
		log.Fatal("Provide path(s) to JSONL files to inline pretty-print")
	}

	files := os.Args[1:]

	processed := false

	var eg errgroup.Group
	for _, f := range files {
		matches, err := filepath.Glob(f)
		must(err)

		for _, match := range matches {
			match := match

			processed = true

			eg.Go(func() error {
				return prettyPrint(match)
			})
		}
	}

	if !processed {
		log.Fatal("Provide path(s) to JSONL files to inline pretty-print")
	}

	must(eg.Wait())
}

func must(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
