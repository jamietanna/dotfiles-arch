#!/usr/bin/env ruby

def extract_xresources_field(colour_name)
  `xrdb -get #{colour_name}`.chomp
end

DATA = `acpi -b`.freeze
remainder = DATA.match(/([0-9]{2}):([0-9]{2}):([0-9]{2})/)
percentage = DATA.match(/([0-9]+)%/)[0]

background = '-'
foreground = '-'

output = ''
charging_indicator = ''

if remainder && DATA.match?(/Discharging/)
  hours = remainder[0].to_i
  mins = remainder[1].to_i

  if percentage.to_i <= 30 ||
      hours == 0 && mins <= 30
    foreground = extract_xresources_field 'color1'
  elsif percentage.to_i <= 10 ||
      hours == 0
    background = extract_xresources_field 'color1'
    foreground = extract_xresources_field 'color15'
  end

else
  charging_indicator = '+'
end

remainder_s = ''
remainder_s = remainder[0] if remainder

output = "%{B#{background}}" +
  "%{F#{foreground}}" +
  percentage +
  ' ' +
  charging_indicator +
  remainder_s +
  '%{F-}%{B-}'

puts output
