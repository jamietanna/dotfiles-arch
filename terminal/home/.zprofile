export EDITOR=nvim
export PATH="$PATH:$HOME/bin:$HOME/go/bin:/home/jamie/.local/bin/"
# make sure we import our custom PATH for any systemd services

# https://stackoverflow.com/a/57591830/2257038
export GPG_TTY=$(tty)
