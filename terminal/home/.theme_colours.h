// vim: foldmethod=marker:foldlevel=0 textwidth=80 wrap
// ----------------------------------------------------------------------------
// .theme_colors.h
//	The colourscheme defined for the system, in a single file that can be easily
//	accessed and parsed by other applications
//	----------------------------------------------------------------------------

// NOTE: The `col_` prefix is required, as the `#define` does C-style
// preprocessor replacement, leading to i.e.
//-*.color0:            color0
//+*.#282828:           #282828
// Base16 definitions {{{
#define _color0 #1C1B19
#define _color1 #EF2F27
#define _color2 #519F50
#define _color3 #FBB829
#define _color4 #2C78BF
#define _color5 #E02C6D
#define _color6 #0AAEB3
#define _color7 #BAA67F
#define _color8 #918175
#define _color9 #F75341
#define _color10 #98BC37
#define _color11 #FED06E
#define _color12 #68A8E4
#define _color13 #FF5C8F
#define _color14 #2BE4D0
#define _color15 #FCE8C3

#define black         _color0
#define red           _color1
#define green         _color2
#define yellow        _color3
#define blue          _color4
#define magenta       _color5
#define cyan          _color6
#define white         _color7
#define brightblack   _color8
#define brightred     _color9
#define brightgreen   _color10
#define brightyellow  _color11
#define brightblue    _color12
#define brightmagenta _color13
#define brightcyan    _color14
#define brightwhite   _color15

#define orange #FF5F00
#define brightorange #FF8700
// }}}

// Helper colours {{{
#define col_focussed      brightgreen
#define col_active        cyan
// }}}
